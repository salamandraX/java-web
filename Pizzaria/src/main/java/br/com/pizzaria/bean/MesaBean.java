package br.com.pizzaria.bean;

import br.com.pizzaria.dao.MesaDAO;
import br.com.pizzaria.domain.Mesa;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;



@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class MesaBean implements Serializable {
	private Mesa mesa;
	private List<Mesa> mesas;

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public List<Mesa> getMesas() {
		return mesas;
	}

	public void setMesas(List<Mesa> mesas) {
		this.mesas = mesas;
	}

	@PostConstruct
	public void listar() {
		try {
			MesaDAO mesaDAO = new MesaDAO();
			mesas = mesaDAO.listar();
		} catch (RuntimeException erro) {
			Messages.
			addGlobalError("Ocorreu um erro ao tentar listar as mesas");
			erro.printStackTrace();
		}
	}

	public void novo() {
		mesa = new Mesa();
	}

	public void salvar() {
		try {
			MesaDAO mesaDAO = new MesaDAO();
			mesaDAO.merge(mesa);

			mesa = new Mesa();
			mesas = mesaDAO.listar();

			Messages.addGlobalInfo("Mesa adicionada");
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar a mesa");
			erro.printStackTrace();
		}
	}

	public void excluir(ActionEvent evento) {
		try {
			mesa = (Mesa) evento.getComponent().
					getAttributes().get("mesaSelecionado");

			MesaDAO mesaDAO = new MesaDAO();
			mesaDAO.excluir(mesa);
			
			mesas = mesaDAO.listar();

			Messages.addGlobalInfo("Mesa removida");
		} catch (RuntimeException erro) {
			Messages.
			addFlashGlobalError("Ocorreu um erro ao tentar remover mesa");
			erro.printStackTrace();
		}
	}
	
	public void editar(ActionEvent evento){
		mesa = (Mesa) evento.
				getComponent().
				getAttributes().get("mesaSelecionado");
	}

}