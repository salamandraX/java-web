package br.com.pizzaria.bean;

import br.com.pizzaria.dao.GarcomDAO;
import br.com.pizzaria.domain.Garcom;
import br.com.pizzaria.domain.Pessoa;
import br.com.pizzaria.dao.PessoaDAO;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;


/**
 *
 * @author heitorap
 */
        
@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class GarcomBean implements Serializable {
    private Garcom garcom;
    private List<Garcom> garcons;
    private List<Pessoa> pessoas;

    public Garcom getGarcom() {
        return garcom;
    }

    public void setGarcom(Garcom garcom) {
        this.garcom = garcom;
    }

    public List<Garcom> getGarcons() {
        return garcons;
    }

    public void setGarcons(List<Garcom> garcons) {
        this.garcons = garcons;
    }

    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }


    

    @PostConstruct
    public void listar() {
            try {
                    GarcomDAO garcomDAO = new GarcomDAO();
                    garcons = garcomDAO.listar();
                    
            } catch (RuntimeException erro) {
                    Messages.
                    addGlobalError("Ocorreu um erro ao tentar listar os pedidos");
                    erro.printStackTrace();
            }
    }

    public void novo() {
            try {
			garcom = new Garcom();

			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoas = pessoaDAO.listar();
			
		} catch (RuntimeException erro) {
			Messages.addFlashGlobalError("Ocorreu um erro ao gerar um novo pedido");
			erro.printStackTrace();
		}
    }

    public void salvar() {
            try {
                    GarcomDAO garcomDAO = new GarcomDAO();
                    garcomDAO.merge(garcom);
                    
                    garcom = new Garcom();
                    
                    PessoaDAO pessoaDAO = new PessoaDAO();
                    pessoas = pessoaDAO.listar();
                    
                    garcons = garcomDAO.listar();
                    
                    Messages.addGlobalInfo("Pessoa adicionada");
            } catch (RuntimeException erro) {
                    Messages.addGlobalError("Ocorreu um erro ao tentar salvar o pedido");
                    erro.printStackTrace();
            }
    }

    public void excluir(ActionEvent evento) {
            try {
                    garcom = (Garcom) evento.getComponent().
                                    getAttributes().get("pedidoSelecionado");
                    

                    GarcomDAO garcomDAO = new GarcomDAO();
                    garcomDAO.excluir(garcom);

                    garcons = garcomDAO.listar();

                    Messages.addGlobalInfo("Garcom removida");
            } catch (RuntimeException erro) {
                    Messages.
                    addFlashGlobalError("Ocorreu um erro ao tentar remover pedido");
                    erro.printStackTrace();
            }
    }

    public void editar(ActionEvent evento){
            try {
                    garcom = (Garcom) evento.getComponent().
                                    getAttributes().get("pedidoSelecionada");
                    
                    PessoaDAO pessoaDAO = new PessoaDAO();
                    pessoas = pessoaDAO.listar();
                    
		} catch (RuntimeException erro) {
                    Messages.addFlashGlobalError("Ocorreu um erro ao tentar selecionar um pedido");
                    erro.printStackTrace();
            }
    }

}

