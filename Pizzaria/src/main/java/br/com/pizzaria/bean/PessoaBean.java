package br.com.pizzaria.bean;

import br.com.pizzaria.dao.PessoaDAO;
import br.com.pizzaria.domain.Pessoa;
import br.com.pizzaria.domain.Mesa;
import br.com.pizzaria.dao.MesaDAO;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;


/**
 *
 * @author heitorap
 */
        
@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class PessoaBean implements Serializable {
    private Pessoa pessoa;
    private List<Pessoa> pessoas;
    private List<Mesa> mesas;


    public List<Mesa> getMesas() {
        return mesas;
    }

    public void setMesas(List<Mesa> mesas) {
        this.mesas = mesas;
    }

    public Pessoa getPessoa() {
            return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
            this.pessoa = pessoa;
    }

    public List<Pessoa> getPessoas() {
            return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
            this.pessoas = pessoas;
    }


    @PostConstruct
    public void listar() {
            try {
                    PessoaDAO pessoaDAO = new PessoaDAO();
                    pessoas = pessoaDAO.listar();
                    
            } catch (RuntimeException erro) {
                    Messages.
                    addGlobalError("Ocorreu um erro ao tentar listar os pedidos");
                    erro.printStackTrace();
            }
    }

    public void novo() {
            try {
			pessoa = new Pessoa();

			MesaDAO mesaDAO = new MesaDAO();
			mesas = mesaDAO.listar();
			
		} catch (RuntimeException erro) {
			Messages.addFlashGlobalError("Ocorreu um erro ao gerar um novo pedido");
			erro.printStackTrace();
		}
    }

    public void salvar() {
            try {
                    PessoaDAO pessoaDAO = new PessoaDAO();
                    pessoaDAO.merge(pessoa);
                    
                    pessoa = new Pessoa();
                    
                    MesaDAO mesaDAO = new MesaDAO();
                    mesas = mesaDAO.listar();
                    
                    pessoas = pessoaDAO.listar();
                    
                    Messages.addGlobalInfo("Pessoa adicionada");
            } catch (RuntimeException erro) {
                    Messages.addGlobalError("Ocorreu um erro ao tentar salvar o pedido");
                    erro.printStackTrace();
            }
    }

    public void excluir(ActionEvent evento) {
            try {
                    pessoa = (Pessoa) evento.getComponent().
                                    getAttributes().get("pedidoSelecionado");
                    

                    PessoaDAO pessoaDAO = new PessoaDAO();
                    pessoaDAO.excluir(pessoa);

                    pessoas = pessoaDAO.listar();

                    Messages.addGlobalInfo("Pessoa removida");
            } catch (RuntimeException erro) {
                    Messages.
                    addFlashGlobalError("Ocorreu um erro ao tentar remover pedido");
                    erro.printStackTrace();
            }
    }

    public void editar(ActionEvent evento){
            try {
                    pessoa = (Pessoa) evento.getComponent().
                                    getAttributes().get("pedidoSelecionada");
                    
                    MesaDAO mesaDAO = new MesaDAO();
                    mesas = mesaDAO.listar();
                    
		} catch (RuntimeException erro) {
                    Messages.addFlashGlobalError("Ocorreu um erro ao tentar selecionar um pedido");
                    erro.printStackTrace();
            }
    }

}

