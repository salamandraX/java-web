/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pizzaria.bean;


import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.pizzaria.dao.ProdutoDAO;
import br.com.pizzaria.dao.MesaDAO;
import br.com.pizzaria.dao.PedidoDAO;
import br.com.pizzaria.domain.Pedido;
import br.com.pizzaria.domain.Produto;
import br.com.pizzaria.domain.Mesa;


@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class PedidoBean {
    private Pedido pedido;
    private List<Pedido> pedidos;
    private List<Produto> produtos;
    private List<Mesa> mesas;

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public List<Mesa> getMesas() {
        return mesas;
    }

    public void setMesas(List<Mesa> mesas) {
        this.mesas = mesas;
    }


   @PostConstruct
    public void listar() {
            try {
                    PedidoDAO pedidoDAO = new PedidoDAO();
                    pedidos = pedidoDAO.listar();
                    
            } catch (RuntimeException erro) {
                    Messages.
                    addGlobalError("Ocorreu um erro ao tentar listar as pessoas");
                    erro.printStackTrace();
            }
    }

    public void novo() {
            try {
			pedido = new Pedido();

			MesaDAO mesaDAO = new MesaDAO();
			mesas = mesaDAO.listar();
                        
                        ProdutoDAO produtoDAO = new ProdutoDAO();
			produtos = produtoDAO.listar();
			
		} catch (RuntimeException erro) {
			Messages.addFlashGlobalError("Ocorreu um erro ao gerar uma nova cidade");
			erro.printStackTrace();
		}
    }

    public void salvar() {
            try {
                    PedidoDAO pedidoDAO = new PedidoDAO();
                    pedidoDAO.merge(pedido);
                    
                    pedido = new Pedido();
                    
                    MesaDAO mesaDAO = new MesaDAO();
                    mesas = mesaDAO.listar();
                    
                    ProdutoDAO produtoDAO = new ProdutoDAO();
                    produtos = produtoDAO.listar();
                    
                    pedidos = pedidoDAO.listar();
                    
                    Messages.addGlobalInfo("Pessoa adicionada");
            } catch (RuntimeException erro) {
                    Messages.addGlobalError("Ocorreu um erro ao tentar salvar a pessoa");
                    erro.printStackTrace();
            }
    }

    public void excluir(ActionEvent evento) {
            try {
                    pedido = (Pedido) evento.getComponent().
                                    getAttributes().get("pessoaSelecionado");
                    

                    PedidoDAO pedidoDAO = new PedidoDAO();
                    pedidoDAO.excluir(pedido);

                    pedidos = pedidoDAO.listar();

                    Messages.addGlobalInfo("Pessoa removida");
            } catch (RuntimeException erro) {
                    Messages.
                    addFlashGlobalError("Ocorreu um erro ao tentar remover pessoa");
                    erro.printStackTrace();
            }
    }

    public void editar(ActionEvent evento){
            try {
                    pedido = (Pedido) evento.getComponent().
                                    getAttributes().get("pessoaSelecionada");
                    
                    MesaDAO mesaDAO = new MesaDAO();
                    mesas = mesaDAO.listar();
                    
                    ProdutoDAO produtoDAO = new ProdutoDAO();
                    produtos = produtoDAO.listar();
                    
		} catch (RuntimeException erro) {
                    Messages.addFlashGlobalError("Ocorreu um erro ao tentar selecionar uma pessoa");
                    erro.printStackTrace();
            }
    }

}
