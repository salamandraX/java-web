/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pizzaria.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author luphnos
 */
@Entity
public class Pessoa extends GenericDomain{
    @Column(length = 50 , nullable = false)
    private String nome;
    @Column(length = 50 , nullable = false, unique = true)
    private String cpf;
    @Column(length = 50 , nullable = false, unique = true)
    private String rg;
    @Column(length = 50 , nullable = false, unique = true)
    private String email;
    @Column(length = 50 , nullable = false, unique = true)
    private String telefone;
    
    @ManyToOne
    @JoinColumn(nullable = true)
    private Mesa mesa;

    public Mesa getMesa() {
        return mesa;
    }

    public void setMesa(Mesa mesa) {
        this.mesa = mesa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

}
