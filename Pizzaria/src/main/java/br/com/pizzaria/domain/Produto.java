/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pizzaria.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author luphnos
 */
@Entity
public class Produto extends GenericDomain{
    @Column(length=50, nullable = false)
    private String nome;
    @Column(length=50, nullable = false)
    private String descricao;
    @Column(length=50, nullable = false)
    private Double preco;

    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }
}
